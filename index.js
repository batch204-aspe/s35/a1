/* TEMPLATE for CREATING A SERVER
	
	const express = require("express");
	const app = express();
	const port = 4000;

	app.use(express.json());
	
	********** Creating a Route Content Start **********
	*										   		   *
	*										   		   *
	*										           *
	*										           *
	*										           *
	*										           *
	********** Creating a Route Content End ************

	app.listen(port, () => console.log(`Server is running at port: ${port}`))
*/

/**/
/*
	Mini-Activity:
		Create an expressJS deignated to port 4000
		Create a new route with endpoint /hello & method GET
			- Should be able to respond with "Hello World"

*/

const express = require("express");
const app = express ();
const port = 4000;

/*
	FOr connecting to MongoDB
		- declare mongoose package

*/

// "Mongoose" is a package that will allow us to create schemas to model our data structures
// Alse has access to a number of methods for manipulating our database
const mongoose = require("mongoose");
/*
	 MongoDB Connection
	 	SYntax:
	 		mongoose.connect("<MongoDB ATlas Connection String>");

	 	Sample Coonection String:
	 		"mongodb+srv://admin:<password>@batch204-aspemarkjoseph.iwwi5jg.mongodb.net/<Database>?retryWrites=true&w=majority"

*/
mongoose.connect("mongodb+srv://admin:admin123@batch204-aspemarkjoseph.iwwi5jg.mongodb.net/B204-to-dos?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Set notification for connection success or failure connection of database
let db = mongoose.connection;
	// If a connection error occured
	db.on("error", console.error.bind(console, "Connection Error"));
	// If the connection is successfuls
	db.once("open", () => console.log("We're Connected to the cloud database."));

/*	Mongoose Schema
		- Schemas determine the structure of the documents to be written in the database

		- Schemas acts as blueprints to our data

		- EveryTime we finished the Schema, we need to create a Model
*/
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});


// Models
// To first parameter of the mongoose model method indicates the db.collectionName where to store the data
// The second parameter is used to specify the schema/blueprint of the documents that will be stored in the MongoDB collection

/*
	syntax:
		const db.collectionNameInSingularForm = mongoose.model("db.collectionNameInSingularFormUpperCase1stLetter", schemaName)

		ex.
			const Task = mongoose.model("Task", taskSchema)

*/
const Task = mongoose.model("Task", taskSchema);


// Automatically Read JSON file whether its a json.stringify or json.parse
// allowing the server to handle data from request
app.use(express.json());


	// ROUTE to create a TAsk
	app.post("/tasks", (req, res) => {

		console.log(req.body);

		//modelName.findOne ► search for the first matching field
		// Check if there is a null or duplicate value
		Task.findOne({name: req.body.name}, (err, result) => {

			if (result !== null && result.name == req.body.name) {

				return res.send("Duplicate Task Found");

			} else {

				let newTask = new Task ({
					name: req.body.name
				});

				newTask.save((saveErr, savedTask) => {
					if (saveErr) {

						return console.error(saveErr);

					} else {
						return res.status(201).send("New Task Created");
					}
				});
			}
		});
	});

	//Create a ROUTE to REtrieve data using Get all the tasks
	app.get("/tasks", (req, res) => {

		Task.find({}, (err, result) => {

			if(err) {

				return console.log(err);

			} else {

				return res.status(200).json({

					data: result

				});
			}

		});

	});

	/********** Actvity Start **********/
	/*
		Activity:
		1. Create a User schema.
		2. Create a User model.
		3. Create a POST route that will access the "/signup" route that will create a user.
		4. Process a POST request at the "/signup" route using postman to register a user.
		5. Create a git repository named S35.
		6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
		7. Add the link in Boodle.

	*/

	// 1. Creating User Schema
		const userSchema = new mongoose.Schema({
			username: String,
			password: String
		});

	// 2. Creating User Model
		const User = mongoose.model("User", userSchema);

	// 3. Create a POST route that will access the "/signup" route that will create a user.
		app.post("/signup", (req, res) => {

			User.findOne({username: req.body.username}, (err, result) => {

				if (result !== null && result.username == req.body.username) {

					return res.send("Duplicate User Found");

				} else {

					let newUser = new User ({
						username: req.body.username,
						password: req.body.password
					});

					newUser.save((saveErr, savedTask) => {
						if (saveErr) {

							return console.error(saveErr);

						} else {
							return res.status(201).send("New User Registered.");
						}
					});
				}
			});

		});
	/********** Actvity End ************/



	// Route Endpoint of "/hello"
	app.get("/hello", (req, res) => {
		res.send("Hello World!");
	});

app.listen(port, () => console.log(`Server is running at port: ${port}`));